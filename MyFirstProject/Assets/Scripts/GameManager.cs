﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject meteor;
    public GameObject enemy;

    private Vector3 startPosition;
    private int lives;

    public Text livesText;

    // Start is called before the first frame update
    void Start()
    {

        startPosition = transform.position;

        BeginGame();

    }

    void BeginGame()
    {
        //Set amount of starting lives to 3
        lives = 3;

        livesText.text = "LIVES: " + lives;

    }

    public void DecrementLives() //Decrement lives each time the player is hit by a meteor or enemy ship
    {
        lives--;
        livesText.text = "LIVES: " + lives;

        //if player's lives drop below 1 the game will quit and the player will be destroyed
        if (lives < 1)
        {
            Application.Quit();

            Destroy(GameObject.Find("Player"));
        }

    }
}
