﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    public static List<GameObject> targets = new List<GameObject>();

    public GameObject meteor;
    public GameObject enemyShip;

    private float numSpawned = 0;
    private float numToSpawn = 3;

    void Start()
    {
        targets.Add(meteor);
        targets.Add(meteor);
        targets.Add(meteor);
        targets.Add(meteor);
        targets.Add(meteor);
        targets.Add(meteor);
        targets.Add(meteor);
        targets.Add(meteor);
        targets.Add(enemyShip);
    }

    void SpawnTarget()
    {
        int whichItem = Random.Range(0, 9);

        GameObject myObj = Instantiate(targets[whichItem]) as GameObject;

        numSpawned++;

        myObj.transform.position = transform.position;
    }

    // Update is called once per frame
    void Update()
    {

        if (numToSpawn > numSpawned)
        { 
            SpawnTarget();
            GetComponent<MeteorController>().OnDestroy();
            GetComponent<EnemyController>().OnDestroy();
        }
    }
}
