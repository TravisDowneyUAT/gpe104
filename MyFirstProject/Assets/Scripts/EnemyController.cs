﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    public AudioClip destroy;
    public GameObject enemy;

    private GameManager gameController;

    private Transform Player;
    public float speed = 3.0f;
    private float minDistance = 0.2f;
    private float range;

    private Transform tf;
    private float turnSpeed = 1.0f;


    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Equals("Bullet"))
        {
            Destroy(GameObject.FindWithTag("Enemy"));
        }

        AudioSource.PlayClipAtPoint(
            destroy, Camera.main.transform.position);
    }


    public void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    public void Update()
    {
        Player = GameObject.FindWithTag("Ship").transform;

        range = Vector2.Distance(transform.position, Player.position);

        if (range > minDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, Player.position,
                speed * Time.deltaTime);
        }

       tf.Rotate(0, 0, turnSpeed);
    }

    public void OnDestroy()
    {
        Instantiate(enemy, new Vector3(10, 0, 0), 
            Quaternion.identity).SetActive(true);
        GameObject.FindWithTag("Enemy").GetComponent<EnemyController>().enabled = true;
        GameObject.FindWithTag("Enemy").GetComponent<PolygonCollider2D>().enabled = true;
    }
}
