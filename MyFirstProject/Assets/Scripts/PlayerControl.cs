﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public AudioClip crash;
    public AudioClip shoot;
    public GameObject bullet;
    private GameManager gameController;

    private float speed = 3.0f;

    // Start is called before the first frame update
    void Start()
    {

        GameObject gameControllerObject =
            GameObject.FindWithTag("GameController");

        gameController =
            gameControllerObject.GetComponent<GameManager>();

    }

    // Update is called once per frame
    void Update()
    {
        //if the player is holding the right arrow down, rotate the player clockwise
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(-Vector3.forward * speed);
        }
        //if the player is holding the left arrow down, rotate the player counter-clockwise
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.forward * speed);
        }
        //if the player is holding the up arrow, the player will move forward in the direction they're facing
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += transform.up * 4.0f * Time.deltaTime;
        }
        //if the player presses the space bar, they will shoot a bullet forward
        if (Input.GetKeyDown(KeyCode.Space))
        {
            ShootBullet();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //if the player comes into contact with the meteor or enemy ship, the player will lose a life
        //and reset to the middle of the screen
        if (collision.gameObject.tag != "Bullet")
        {
            AudioSource.PlayClipAtPoint
                (crash, Camera.main.transform.position);

            transform.position = new Vector3(0, 0, 0);

            GetComponent<Rigidbody2D>().velocity = new Vector3(0, 0, 0);

            gameController.DecrementLives();

        }

    }

    void ShootBullet()
    {
        //instantiates a bullet prefab when the player presses the space bar
        Instantiate(bullet,
            new Vector3(transform.position.x, transform.position.y, 0),
            transform.rotation);

        AudioSource.PlayClipAtPoint(shoot, Camera.main.transform.position);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(collision.gameObject);
    }

    public void OnBecameInvisible() //destroys the player if they go outside the boundaries of the camera
    {
        Destroy(gameObject);
    }
}
