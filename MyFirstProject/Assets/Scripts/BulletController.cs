﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //Set the bullet object to destroy itself after 1 second
        Destroy(gameObject, 1.0f);

        //push the bullet in the direction it is facing
        GetComponent<Rigidbody2D>().AddForce(transform.up * 400);

    }
}
