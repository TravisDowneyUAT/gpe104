﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorController : MonoBehaviour
{

    public AudioClip destroy;
    public GameObject meteor;

    private GameManager gameController;

    public Transform Player;
    public float speed = 1.0f;
    private float minDistance = 0.2f;
    private float range;

    // Start is called before the first frame update
    void Start()
    {

        GameObject gameControllerObject =
            GameObject.FindWithTag("GameController");
        gameController =
            gameControllerObject.GetComponent<GameManager>();

        GetComponent<Rigidbody2D>().AddForce(transform.up * Random.Range(-50.0f, 150.0f));

        GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-0.0f, 90.0f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Destroy meteo object if it is hit by a bullet object
        if (collision.gameObject.tag.Equals("Bullet"))
        {
            Destroy(GameObject.FindWithTag("Meteor"));
        }

        //play an audio file when the meteor is destroyed
        AudioSource.PlayClipAtPoint(
            destroy, Camera.main.transform.position);

    }

    public void OnBecameInvisible() //If the meteor goes off screen, destroy it
    {
        Destroy(gameObject);
    }

    public void Update()
    {
        Player = GameObject.FindWithTag("Ship").transform;

        range = Vector2.Distance(transform.position, Player.position);

        if(range > minDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, Player.position,
                speed * Time.deltaTime);
        }
    }
    public void OnDestroy()
    {
        Instantiate(meteor, new Vector3(10, 0, 0),
            Quaternion.identity).SetActive(true);
        GameObject.FindWithTag("Meteor").GetComponent<MeteorController>().enabled = true;
        GameObject.FindWithTag("Meteor").GetComponent<PolygonCollider2D>().enabled = true;
    }
}
